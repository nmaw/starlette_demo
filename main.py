from starlette.applications import Starlette
from starlette.responses import PlainTextResponse, HTMLResponse
from starlette.routing import Route, Mount, WebSocketRoute
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
import jinja2


APP = Starlette(debug=True)
APP.mount('/static', StaticFiles(directory='static'), name='static')
TEMPLATE = Jinja2Templates(directory='templates')


# @APP.route('/')
# async def homepage(request):
#     template = "index.html"
#     context = {"request": request}
#     return TEMPLATE.TemplateResponse(template, context)


@APP.route('/home')
def homepage(request):
    return HTMLResponse('''<div class="alert alert-primary" role="alert">A simple primary alert—check it out!</div>''')


@APP.route('/user/me')
def user_me(request):
    username = "John Doe"
    return PlainTextResponse('Hello, %s!' % username)


@APP.route('/user/<name>')
def user(request):
    username = request.path_params['username']
    return PlainTextResponse('Hello, %s!' % username)


# @APP.websocket_route('/ws')
# async def websocket_endpoint(websocket):
#     await websocket.accept()
#     rec = await websocket.receive_text()
#     print('BackEnd received message: {}'.format(rec))
#     await websocket.send_text('Hello, websocket!')
#     await websocket.close()


def startup():
    print('Ready to go')


# app = Starlette(debug=True, routes=routes, on_startup=[startup])

