FROM python:3.8.2-slim-buster

WORKDIR /root

RUN pip install starlette hypercorn aiofiles jinja2