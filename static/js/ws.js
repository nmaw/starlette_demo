document.forms.publish.onsubmit = function () {
    let outgoingMessage = this.message.value;

    ws.send(outgoingMessage);
    return false;
};

function getTheDate(){
    var rigthNow = new Date();
    var answer = {
    time: rigthNow.toTimeString(),
    date: rigthNow.toDateString()
    }
    return answer
}

function WebSocketTest() {
    if ("WebSocket" in window) {
        var ws = new WebSocket("ws://localhost/ws");
        console.log("Web socket work")

        ws.onopen = function () {
            now = getTheDate()
            message = now.time + " Message from FrontEnd to send!"
            ws.send(message);
            console.log(message);
        };


        ws.onmessage = function (evt) {
            var received_msg = evt.data;
            now = getTheDate()
            console.log("Message is received..." + now);
            var mes = document.getElementById("messages")
            mes.append.innerHTML("<br>" + received_msg + now)
        };

        //      ws.onclose = function() {
        //      alert("Connection is closed...");
        //      };
    } else {
        // The browser doesn't support WebSocket
        alert("WebSocket NOT supported by your Browser!");
    }
}